package com.azrinaziz.recipe

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import jp.wasabeef.recyclerview.animators.LandingAnimator
import kotlinx.android.synthetic.main.activity_recipes.*

class RecipesActivity : AppCompatActivity() {

    private val adapter = RecipeAdapter()
    private val model by viewModels<Model>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipes)
        setSupportActionBar(tb_recipes)

        rv_recipes.also {
            it.layoutManager = LinearLayoutManager(this)
            it.itemAnimator = LandingAnimator()
            it.adapter = adapter
        }

        sr_recipes.apply {
            setOnRefreshListener { model.refreshRecipes() }
            setColorSchemeResources(
                R.color.primaryLightColor,
                R.color.primaryColor,
                R.color.primaryDarkColor
            )
        }

        model.recipes.observe(this, Observer {
            Log.i("Recipes", Gson().toJson(it))

            adapter.removeAll()
            if (it.isNotEmpty()) adapter.add(it)
            if (sr_recipes.isRefreshing) sr_recipes.isRefreshing = false
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_ADD_RECIPE && resultCode == Activity.RESULT_OK) {
            data?.extras?.getString(KEY_RECIPE_NAME).apply {
                Snackbar.make(
                    sr_recipes,
                    getString(R.string.add_recipe_success, this),
                    Snackbar.LENGTH_SHORT
                ).show()
            }

            model.refreshRecipes()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_recipes, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.action_add_recipe -> {
                Intent(this, RecipeAddActivity::class.java).apply {
                    startActivityForResult(this, RC_ADD_RECIPE)
                }
            }

            R.id.action_filter_by_recipe_type -> {
                AlertDialog.Builder(this).apply {
                    setItems(getRecipeTypes(this@RecipesActivity).let { recipeTypes ->
                        mutableListOf<String>().let {
                            for (value in recipeTypes) {
                                it.add(value.name)
                            }

                            it.add(getString(R.string.show_all_recipes))
                            it.toTypedArray()
                        }
                    }) { _, i ->
                        getRecipeTypes(this@RecipesActivity).also {
                            setFilter(this@RecipesActivity, (if (i < it.size) it[i] else null))
                            model.refreshRecipes()
                        }
                    }
                    setNeutralButton(R.string.button_back, null)
                    setTitle(R.string.filter_recipes_by)
                }.create().show()
            }
        }

        return super.onOptionsItemSelected(menuItem)
    }

    override fun onPause() {
        super.onPause()
        adapter.removeAll()
    }

    override fun onResume() {
        super.onResume()
        model.refreshRecipes()
    }

    companion object {
        internal const val KEY_RECIPE = "recipe"
        internal const val KEY_RECIPE_NAME = "recipe_name"
        private const val RC_ADD_RECIPE = 1606
    }

    internal class Model(application: Application) : AndroidViewModel(application) {

        val recipes = MutableLiveData<Array<Recipe>>()

        init {
            setFilter(getApplication(), null)
        }

        internal fun refreshRecipes() {
            val recipeType = getFilter(getApplication())

            getRecipes(getApplication()).apply {
                if (recipeType != null) {
                    mutableListOf<Recipe>().also {
                        for (recipe in this) {
                            if (recipe.type == recipeType) it.add(recipe)
                        }

                        recipes.value = it.toTypedArray()
                    }
                } else {
                    recipes.value = this.toTypedArray()
                }
            }
        }
    }
}
