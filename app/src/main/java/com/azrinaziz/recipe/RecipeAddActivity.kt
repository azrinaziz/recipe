package com.azrinaziz.recipe

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_recipe_add.*
import okio.buffer
import okio.source

open class RecipeAddActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_add)
        setSupportActionBar(tb_recipeAdd)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_24dp)

        ac_recipeType.setAdapter(ArrayAdapter(
            this,
            R.layout.dropdown_recipe_type,
            getRecipeTypes(this).run {
                val recipeType = mutableListOf<String>()

                for (recipeTypeClass in this) {
                    recipeType.add(recipeTypeClass.name)
                }

                recipeType.toTypedArray()
            }
        ))

        bt_filePicker.setOnClickListener {
            Intent(Intent.ACTION_OPEN_DOCUMENT).also {
                it.addCategory(Intent.CATEGORY_OPENABLE)
                it.type = "image/*"
                startActivityForResult(it, RC_FILE_PICKER)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_FILE_PICKER && resultCode == Activity.RESULT_OK) {
            data?.data.also {
                te_imagePath.setText("$it")
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_add_recipe, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                finish()
            }

            R.id.action_save -> saveRecipe()
        }

        return super.onOptionsItemSelected(menuItem)
    }

    internal open fun saveRecipe() {
        when {
            te_recipeName.text.isNullOrEmpty() -> {
                ti_recipeName.error = getString(R.string.error_empty_recipe_name)
                te_recipeName.requestFocus()
            }
            ac_recipeType.text.isEmpty() -> {
                ti_recipeType.error = getString(R.string.error_empty_recipe_type)
                ac_recipeType.requestFocus()
            }
            te_imagePath.text.isNullOrEmpty() -> {
                ti_imagePath.error = getString(R.string.error_empty_picture)
            }
            te_ingredients.text.isNullOrEmpty() -> {
                ti_ingredients.error = getString(R.string.error_empty_ingredients)
                te_ingredients.requestFocus()
            }
            te_steps.text.isNullOrEmpty() -> {
                ti_steps.error = getString(R.string.error_empty_steps)
                te_steps.requestFocus()
            }
            else -> {
                Recipe(
                    te_recipeName.text.toString(),
                    ac_recipeType.text.toString().let { type ->
                        getRecipeTypes(this).let {
                            var index = 0

                            for (recipeType in it) {

                                if (recipeType.name == type) {
                                    index = it.indexOf(recipeType)
                                    break
                                }
                            }

                            it[index]
                        }
                    },
                    te_imagePath.text.toString().run {
                        contentResolver.openInputStream(Uri.parse(this)).run {
                            Base64.encodeToString(
                                this?.source()?.buffer()?.readByteArray(),
                                Base64.DEFAULT
                            )
                        }
                    },
                    te_ingredients.text.toString(),
                    te_steps.text.toString()
                ).also {
                    getRecipes(this).apply {
                        add(it)
                        setRecipes(this@RecipeAddActivity, this)
                    }

                    Intent().apply {
                        putExtra(RecipesActivity.KEY_RECIPE_NAME, it.name)
                        setResult(Activity.RESULT_OK, this)
                        finish()
                    }
                }
            }
        }
    }

    companion object {
        private const val RC_FILE_PICKER = 1606
    }
}
