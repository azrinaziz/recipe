package com.azrinaziz.recipe

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_recipe.*

class RecipeActivity : AppCompatActivity() {

    private lateinit var recipe: Recipe
    private var recipeIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe)
        setSupportActionBar(tb_recipe)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_24dp)

        intent.extras?.also {
            recipeIndex = it.getInt(RecipesActivity.KEY_RECIPE)
            recipe = getRecipes(this)[recipeIndex]
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_EDIT_RECIPE && resultCode == Activity.RESULT_OK) {
            data?.extras?.getString(RecipesActivity.KEY_RECIPE_NAME).apply {
                recipe = getRecipes(this@RecipeActivity)[recipeIndex]

                Snackbar.make(
                    ll_recipe,
                    getString(R.string.edit_recipe_success, this),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_recipe, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> finish()

            R.id.action_edit -> {
                Intent(this, RecipeEditActivity::class.java).apply {
                    putExtra(
                        RecipesActivity.KEY_RECIPE,
                        getRecipes(this@RecipeActivity).indexOf(recipe)
                    )

                    startActivityForResult(this, RC_EDIT_RECIPE)
                }
            }
        }

        return super.onOptionsItemSelected(menuItem)
    }

    override fun onResume() {
        super.onResume()
        displayRecipe(recipe)
    }

    private fun displayRecipe(recipe: Recipe) {
        val image = Base64.decode(recipe.image, Base64.DEFAULT)

        BitmapFactory.decodeByteArray(image, 0, image.size).also { bitmap ->
            iv_recipe.setImageBitmap(
                Bitmap.createScaledBitmap(
                    bitmap,
                    bitmap.width,
                    bitmap.height,
                    false
                )
            )
        }

        supportActionBar?.title = recipe.name
        tv_ingredients.text = recipe.ingredients
        tv_steps.text = recipe.steps
    }

    companion object {
        private const val RC_EDIT_RECIPE = 1606
    }
}
