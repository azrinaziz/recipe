package com.azrinaziz.recipe

import androidx.annotation.Keep

@Keep
internal data class Recipe(
    var name: String,
    var type: RecipeType,
    var image: String,
    val ingredients: String,
    val steps: String
)
