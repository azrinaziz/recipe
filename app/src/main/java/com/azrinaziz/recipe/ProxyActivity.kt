package com.azrinaziz.recipe

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class ProxyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(this, RecipesActivity::class.java))
        finish()
    }
}
