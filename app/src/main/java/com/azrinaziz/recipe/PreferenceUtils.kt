package com.azrinaziz.recipe

import android.content.Context
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import okio.buffer
import okio.source

private const val FILTER_KEY = "filter"
private const val RECIPE_KEY = "recipes"

internal fun getFilter(context: Context) = Gson().fromJson(
    sharedPreferences(context).getString(FILTER_KEY, null),
    RecipeType::class.java
)

internal fun getRecipes(context: Context) = Gson().fromJson(
    sharedPreferences(context).getString(
        RECIPE_KEY,
        context.assets.open("recipes.json").source().buffer().readUtf8()
    ),
    Array<Recipe>::class.java
).toMutableList()

internal fun setFilter(context: Context, recipeType: RecipeType?) {
    sharedPreferences(context).edit {
        putString(FILTER_KEY, if (recipeType != null) Gson().toJson(recipeType) else null)
    }
}

internal fun setRecipes(context: Context, recipes: MutableList<Recipe>) {
    sharedPreferences(context).edit {
        putString(RECIPE_KEY, Gson().toJson(recipes.toTypedArray()))
    }
}

private fun sharedPreferences(context: Context) = PreferenceManager.getDefaultSharedPreferences(
    context
)
