package com.azrinaziz.recipe

import androidx.annotation.Keep

@Keep
internal data class RecipeType(val id: Int) {
    var name: String = ""
}
