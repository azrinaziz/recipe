package com.azrinaziz.recipe

import android.content.Context
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory

internal fun getRecipeTypes(context: Context): Array<RecipeType> {
    val recipeTypes = mutableListOf<RecipeType>()

    XmlPullParserFactory.newInstance().apply {
        newPullParser().apply {
            var element: String
            var eventType = eventType
            var recipeType: RecipeType? = null

            setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            setInput(context.assets.open("recipetypes.xml"), null)

            while (eventType != XmlPullParser.END_DOCUMENT) {

                if (eventType == XmlPullParser.START_TAG) {
                    element = name

                    when {
                        element == "recipetype" -> {
                            recipeType = RecipeType(getAttributeValue(null, "id").toInt())
                            recipeTypes.add(recipeType)
                        }

                        recipeType != null -> if (element == name) recipeType.name = nextText()
                    }
                }

                eventType = next()
            }
        }
    }

    return recipeTypes.toTypedArray()
}
