package com.azrinaziz.recipe

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_recipe_add.*
import okio.buffer
import okio.source

class RecipeEditActivity : RecipeAddActivity() {

    private lateinit var recipe: Recipe
    private var recipeIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        intent.extras?.also {
            recipeIndex = it.getInt(RecipesActivity.KEY_RECIPE)

            getRecipes(this)[recipeIndex].apply {
                recipe = this
                supportActionBar?.title = getString(R.string.edit_recipe, name)

                te_recipeName.setText(name)
                te_ingredients.setText(ingredients)
                te_steps.setText(steps)

                ac_recipeType.setText(type.name)
                ac_recipeType.setAdapter(ArrayAdapter(
                    this@RecipeEditActivity,
                    R.layout.dropdown_recipe_type,
                    getRecipeTypes(this@RecipeEditActivity).run {
                        val recipeType = mutableListOf<String>()

                        for (recipeTypeClass in this) {
                            recipeType.add(recipeTypeClass.name)
                        }

                        recipeType.toTypedArray()
                    }
                ))
            }
        }
    }

    override fun saveRecipe() {
        when {
            te_recipeName.text.isNullOrEmpty() -> {
                ti_recipeName.error = getString(R.string.error_empty_recipe_name)
                te_recipeName.requestFocus()
            }
            ac_recipeType.text.isEmpty() -> {
                ti_recipeType.error = getString(R.string.error_empty_recipe_type)
                ac_recipeType.requestFocus()
            }
            te_ingredients.text.isNullOrEmpty() -> {
                ti_ingredients.error = getString(R.string.error_empty_ingredients)
                te_ingredients.requestFocus()
            }
            te_steps.text.isNullOrEmpty() -> {
                ti_steps.error = getString(R.string.error_empty_steps)
                te_steps.requestFocus()
            }
            else -> {
                Recipe(
                    te_recipeName.text.toString(),
                    ac_recipeType.text.toString().let { type ->
                        getRecipeTypes(this).let {
                            var index = 0

                            for (recipeType in it) {

                                if (recipeType.name == type) {
                                    index = it.indexOf(recipeType)
                                    break
                                }
                            }

                            it[index]
                        }
                    },
                    if (!te_imagePath.text.isNullOrEmpty()) {
                        te_imagePath.text.toString().run {
                            contentResolver.openInputStream(Uri.parse(this)).run {
                                Base64.encodeToString(
                                    this?.source()?.buffer()?.readByteArray(),
                                    Base64.DEFAULT
                                )
                            }
                        }
                    } else {
                        recipe.image
                    },
                    te_ingredients.text.toString(),
                    te_steps.text.toString()
                ).also {
                    getRecipes(this).apply {
                        this[recipeIndex] = it
                        setRecipes(this@RecipeEditActivity, this)
                    }

                    Intent().apply {
                        putExtra(RecipesActivity.KEY_RECIPE_NAME, it.name)
                        setResult(Activity.RESULT_OK, this)
                        finish()
                    }
                }
            }
        }
    }
}
