package com.azrinaziz.recipe

import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.adapter_recipe.view.*

internal class RecipeAdapter : RecyclerView.Adapter<RecipeAdapter.ViewHolder>() {

    private val recipes = mutableListOf<Recipe>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.adapter_recipe, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(this, recipes[position])
    }

    override fun getItemCount(): Int {
        return recipes.size
    }

    fun add(recipes: Array<Recipe>) {
        for (recipe in recipes) {
            add(recipe)
        }
    }

    fun remove(position: Int) {
        recipes.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, recipes.size)
    }

    fun removeAll() {
        val position = 0

        if (recipes.size > 0) {
            recipes.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, recipes.size)
            removeAll()
        }
    }

    private fun add(recipe: Recipe) {
        recipes.add(recipe)
        notifyItemInserted(recipes.indexOf(recipe))
    }

    internal class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(recipeAdapter: RecipeAdapter, recipe: Recipe) {
            val listener = DialogInterface.OnClickListener { _, _ ->
                val recipes = getRecipes(itemView.context)
                val deletedRecipe = recipes[adapterPosition]

                recipeAdapter.remove(adapterPosition)
                recipes.remove(deletedRecipe)
                setRecipes(itemView.context, recipes)

                Snackbar.make(
                    itemView,
                    R.string.delete_recipe_success,
                    Snackbar.LENGTH_LONG
                ).setAction(R.string.button_undo) {
                    recipeAdapter.add(deletedRecipe)
                    recipes.add(deletedRecipe)
                    setRecipes(itemView.context, recipes)
                }.show()
            }

            itemView.im_deleteRecipe.setOnClickListener {
                AlertDialog.Builder(itemView.context).apply {
                    setMessage(R.string.delete_recipe_prompt)
                    setPositiveButton(R.string.button_yes, listener)
                    setNegativeButton(R.string.button_no, null)
                    create().show()
                }
            }

            itemView.ll_recipe.setOnClickListener {
                Intent(itemView.context, RecipeActivity::class.java).apply {
                    putExtra(
                        RecipesActivity.KEY_RECIPE,
                        getRecipes(itemView.context).indexOf(recipe)
                    )

                    itemView.context.startActivity(this)
                }
            }

            itemView.tv_recipeName.text = recipe.name
        }
    }
}
